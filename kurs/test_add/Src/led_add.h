#ifndef LED_ADD_H
#define LED_ADD_H

#include "stm32f1xx_hal.h"

#define zer 12
#define one 33
#define led_count 17															//���������� �����������
#define bit_count 24															//���������� ��� �� 1 ���������
#define delay_led 80															//���������� ��� ��� ��������
#define array_len led_count*bit_count+delay_led		//������ ������ ������
#define step_until 50
#define step 5

#define el_on() HAL_GPIO_WritePin(EL_GPIO_Port,EL_Pin,GPIO_PIN_SET)
#define el_off() HAL_GPIO_WritePin(EL_GPIO_Port,EL_Pin,GPIO_PIN_RESET)

#define BitIsSet(reg, bit) ((reg & (1<<bit)) != 0)

void led_init (void);
void set_led (uint8_t Gbit,uint8_t Rbit,uint8_t Bbit,uint8_t poz);
void mode (uint8_t k[3]);
void mode1_init (uint8_t p);
void mode23_init (void);
void mode4_init (void);
void set_settings (uint8_t q[4]);

struct SETTINGS
{
	uint8_t mod;
	uint8_t grb;
	uint8_t intens;
	uint8_t el;
};

struct MODS
{
	uint8_t flag;
	uint8_t intens[3];
	uint8_t grb;
};
#endif
