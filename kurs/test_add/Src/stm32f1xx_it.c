/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32f1xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "led_add.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */
extern struct MODS mods;
extern struct SETTINGS settings;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern DMA_HandleTypeDef hdma_tim3_ch4_up;
extern TIM_HandleTypeDef htim4;
extern UART_HandleTypeDef huart3;
/* USER CODE BEGIN EV */
extern TIM_HandleTypeDef htim3;
/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M3 Processor Interruption and Exception Handlers          */ 
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */

  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Memory management fault.
  */
void MemManage_Handler(void)
{
  /* USER CODE BEGIN MemoryManagement_IRQn 0 */

  /* USER CODE END MemoryManagement_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_MemoryManagement_IRQn 0 */
    /* USER CODE END W1_MemoryManagement_IRQn 0 */
  }
}

/**
  * @brief This function handles Prefetch fault, memory access fault.
  */
void BusFault_Handler(void)
{
  /* USER CODE BEGIN BusFault_IRQn 0 */

  /* USER CODE END BusFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_BusFault_IRQn 0 */
    /* USER CODE END W1_BusFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Undefined instruction or illegal state.
  */
void UsageFault_Handler(void)
{
  /* USER CODE BEGIN UsageFault_IRQn 0 */

  /* USER CODE END UsageFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_UsageFault_IRQn 0 */
    /* USER CODE END W1_UsageFault_IRQn 0 */
  }
}

/**
  * @brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{
  /* USER CODE BEGIN SVCall_IRQn 0 */

  /* USER CODE END SVCall_IRQn 0 */
  /* USER CODE BEGIN SVCall_IRQn 1 */

  /* USER CODE END SVCall_IRQn 1 */
}

/**
  * @brief This function handles Debug monitor.
  */
void DebugMon_Handler(void)
{
  /* USER CODE BEGIN DebugMonitor_IRQn 0 */

  /* USER CODE END DebugMonitor_IRQn 0 */
  /* USER CODE BEGIN DebugMonitor_IRQn 1 */

  /* USER CODE END DebugMonitor_IRQn 1 */
}

/**
  * @brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{
  /* USER CODE BEGIN PendSV_IRQn 0 */

  /* USER CODE END PendSV_IRQn 0 */
  /* USER CODE BEGIN PendSV_IRQn 1 */

  /* USER CODE END PendSV_IRQn 1 */
}

/**
  * @brief This function handles System tick timer.
  */
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32F1xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f1xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles DMA1 channel3 global interrupt.
  */
void DMA1_Channel3_IRQHandler(void)
{
  /* USER CODE BEGIN DMA1_Channel3_IRQn 0 */

  /* USER CODE END DMA1_Channel3_IRQn 0 */
  HAL_DMA_IRQHandler(&hdma_tim3_ch4_up);
  /* USER CODE BEGIN DMA1_Channel3_IRQn 1 */

  /* USER CODE END DMA1_Channel3_IRQn 1 */
}

/**
  * @brief This function handles TIM4 global interrupt.
  */
void TIM4_IRQHandler(void)
{
  /* USER CODE BEGIN TIM4_IRQn 0 */

  /* USER CODE END TIM4_IRQn 0 */
  HAL_TIM_IRQHandler(&htim4);
  /* USER CODE BEGIN TIM4_IRQn 1 */
	if (settings.mod==1){mode(mods.intens);}
	else if (settings.mod==2)
	{
		if (settings.grb==1||settings.grb==4||settings.grb==5||settings.grb==7)
		{
			if (mods.intens[0]==settings.intens&&mods.flag==0){mods.flag=1;}
			else if (mods.intens[0]==0&&mods.flag==1){mods.flag=0;}
			if (mods.flag==0){mods.intens[0]++;}
			else if (mods.flag==1){mods.intens[0]=mods.intens[0]-1;}
		}
		if (settings.grb==2||settings.grb==4||settings.grb==6||settings.grb==7)
		{
			if (mods.intens[1]==settings.intens&&mods.flag==0){mods.flag=1;}
			else if (mods.intens[1]==0&&mods.flag==1){mods.flag=0;}
			if (mods.flag==0){mods.intens[1]++;}
			else if (mods.flag==1){mods.intens[1]=mods.intens[1]-1;}
		}
		if (settings.grb==3||settings.grb==5||settings.grb==6||settings.grb==7)
		{
			if (mods.intens[2]==settings.intens&&mods.flag==0){mods.flag=1;}
			else if (mods.intens[2]==0&&mods.flag==1){mods.flag=0;}
			if (mods.flag==0){mods.intens[2]++;}
			else if (mods.flag==1){mods.intens[2]=mods.intens[2]-1;}
		}
		mode(mods.intens);
	}
	else if (settings.mod==3)
	{
		if (mods.grb==1)
		{
			if (mods.flag==0){mods.intens[0]++;}
			else if (mods.flag==1){mods.intens[0]=mods.intens[0]-1;}
			if (mods.intens[0]==settings.intens&&mods.flag==0){mods.flag=1;}
			else if (mods.intens[0]==0&&mods.flag==1){mods.flag=0;mods.grb++;}
		}
		if (mods.grb==2)
		{
			if (mods.flag==0){mods.intens[1]++;}
			else if (mods.flag==1){mods.intens[1]=mods.intens[1]-1;}
			if (mods.intens[1]==settings.intens&&mods.flag==0){mods.flag=1;}
			else if (mods.intens[1]==0&&mods.flag==1){mods.flag=0;mods.grb++;}
		}
		if (mods.grb==3)
		{
			if (mods.flag==0){mods.intens[2]++;}
			else if (mods.flag==1){mods.intens[2]=mods.intens[2]-1;}
			if (mods.intens[2]==settings.intens&&mods.flag==0){mods.flag=1;}
			else if (mods.intens[2]==0&&mods.flag==1){mods.flag=0;mods.grb++;}
		}
		if (mods.grb==4){mods.grb=1;}
		mode(mods.intens);
	}
	else if (settings.mod==4)
	{
		if (mods.grb==1)
		{
			if (mods.intens[0]>=30){mods.intens[0]=mods.intens[0]-1;}
			else if (mods.intens[0]>0&&mods.intens[0]<30){mods.intens[1]++;mods.intens[0]=mods.intens[0]-1;}
			else if (mods.intens[0]==0&&mods.intens[1]==step_until){mods.grb++;}else{mods.intens[1]++;}
		}
		if (mods.grb==2)
		{
			if (mods.intens[1]>=30){mods.intens[1]=mods.intens[1]-1;}
			else if (mods.intens[1]>0&&mods.intens[1]<30){mods.intens[2]++;mods.intens[1]=mods.intens[1]-1;}
			else if (mods.intens[1]==0&&mods.intens[2]==step_until){mods.grb++;}else{mods.intens[2]++;}
		}
		if (mods.grb==3)
		{
			if (mods.intens[2]>=30){mods.intens[2]=mods.intens[2]-1;}
			else if (mods.intens[2]>0&&mods.intens[2]<30){mods.intens[0]++;mods.intens[2]=mods.intens[2]-1;}
			else if (mods.intens[2]==0&&mods.intens[0]==step_until){mods.grb=1;}else{mods.intens[0]++;}
		}
		mode(mods.intens);
	}
  /* USER CODE END TIM4_IRQn 1 */
}

/**
  * @brief This function handles USART3 global interrupt.
  */
void USART3_IRQHandler(void)
{
  /* USER CODE BEGIN USART3_IRQn 0 */

  /* USER CODE END USART3_IRQn 0 */
  HAL_UART_IRQHandler(&huart3);
  /* USER CODE BEGIN USART3_IRQn 1 */

  /* USER CODE END USART3_IRQn 1 */
}

/* USER CODE BEGIN 1 */

/* USER CODE END 1 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
