#include "led_add.h"
#include "main.h"
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;
extern UART_HandleTypeDef huart3;
uint16_t buff_led[array_len]={0};	//����� ������ ��� �����
uint8_t set[4]={0};
struct MODS mods;
struct SETTINGS settings;

void set_settings (uint8_t q[4])
{
	settings.el=q[3];
	if (settings.el==1){el_on();}else if (settings.el==0){el_off();}
	if (settings.mod!=q[0]||settings.grb!=q[1]||settings.intens!=q[2])
	{
		settings.mod=q[0];
		settings.grb=q[1];
		settings.intens=q[2];
		HAL_TIM_Base_Stop_IT(&htim4);
		led_init();
		if (settings.mod!=0)
		{
			if (settings.mod==1){mode1_init(settings.intens);}
			else if (settings.mod==2||settings.mod==3){mode23_init();if (settings.mod==3){mods.grb=1;}}
			else if (settings.mod==4){mode4_init();mods.grb=1;}
			mods.flag=0;
			HAL_TIM_Base_Start_IT(&htim4);
		}
	}
}
void led_init (void)
{
	for (int i=0;i<array_len-delay_led;i++){buff_led[i]=zer;}
	HAL_TIM_PWM_Start_DMA(&htim3,TIM_CHANNEL_4,(uint32_t*)&buff_led,array_len);
}
void set_led (uint8_t Gbit,uint8_t Rbit,uint8_t Bbit,uint8_t poz)
{
	for (int i=0;i<8;i++)
	{
		if (BitIsSet(Gbit,(7-i))==1){buff_led[poz*24+i]=one;}else{buff_led[poz*24+i]=zer;}
		if (BitIsSet(Rbit,(7-i))==1){buff_led[poz*24+i+8]=one;}else{buff_led[poz*24+i+8]=zer;}
		if (BitIsSet(Bbit,(7-i))==1){buff_led[poz*24+i+16]=one;}else{buff_led[poz*24+i+16]=zer;}
	}
}
void mode1_init (uint8_t p)
{
	if (settings.grb==1){mods.intens[0]=p;mods.intens[1]=0;mods.intens[2]=0;}
	else if (settings.grb==2){mods.intens[0]=0;mods.intens[1]=p;mods.intens[2]=0;}
	else if (settings.grb==3){mods.intens[0]=0;mods.intens[1]=0;mods.intens[2]=p;}
	else if (settings.grb==4){mods.intens[0]=p;mods.intens[1]=p;mods.intens[2]=0;}
	else if (settings.grb==5){mods.intens[0]=p;mods.intens[1]=0;mods.intens[2]=p;}
	else if (settings.grb==6){mods.intens[0]=0;mods.intens[1]=p;mods.intens[2]=p;}
	else if (settings.grb==7){mods.intens[0]=p;mods.intens[1]=p;mods.intens[2]=p;}
}
void mode23_init (void){mods.intens[0]=0;mods.intens[1]=0;mods.intens[2]=0;}
void mode4_init (void){mods.intens[0]=step_until;mods.intens[1]=0;mods.intens[2]=0;}
void mode (uint8_t k[3])
{
	for (int i=0;i<led_count;i++){set_led(k[0]*step,k[1]*step,k[2]*step,i);}
	HAL_TIM_PWM_Start_DMA(&htim3,TIM_CHANNEL_4,(uint32_t*)&buff_led,array_len);
}
