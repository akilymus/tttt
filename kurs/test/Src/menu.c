#include "stm32f1xx_hal.h"
#include "main.h"
#include "menu.h"
#include "lcd.h"

struct BUTTONS flags_buttons;
struct MENU lvl_menu;
uint8_t set[4]={0};
void draw_lvl (void)
{
	if (lvl_menu.menu_poz==0||lvl_menu.menu_poz==2||lvl_menu.menu_poz==4||lvl_menu.menu_poz==6)
	{
		char qwe;
		set_DDRAM_address(0x00);
		send_str(lvl_menu.mode);
		qwe=set[0]+'0';
		write_data(qwe);
		set_DDRAM_address(0x40);
		send_str(lvl_menu.grb);
		qwe=set[1]+'0';
		write_data(qwe);
		set_DDRAM_address(0x08);
		send_str(lvl_menu.intens);
		if (set[2]>9){qwe=set[2]/10+'0';write_data(qwe);qwe=set[2]-set[2]/10*10+'0';write_data(qwe);}else {qwe=set[2]+'0';write_data(qwe);}
		set_DDRAM_address(0x48);
		send_str(lvl_menu.el);
		if (set[3]==0){send_str("OFF");}else if (set[3]==1){send_str("ON");}
	}
	else if (lvl_menu.menu_poz==1)
	{
		set_DDRAM_address(0x00);
		send_str(lvl_menu.mode1);
		set_DDRAM_address(0x40);
		send_str("0");
		set_DDRAM_address(0x43);
		send_str("1");
		set_DDRAM_address(0x46);
		send_str("2");
		set_DDRAM_address(0x49);
		send_str("3");
		set_DDRAM_address(0x4C);
		send_str("4");
		if (set[0]==0){lvl_menu.addres=0x41;}else if (set[0]==1){lvl_menu.addres=0x44;}else if (set[0]==2){lvl_menu.addres=0x47;}else if (set[0]==3){lvl_menu.addres=0x4A;}else if (set[0]==4){lvl_menu.addres=0x4D;}
	}
	else if (lvl_menu.menu_poz==3)
	{
		set_DDRAM_address(0x00);
		send_str(lvl_menu.grb1);
		set_DDRAM_address(0x40);
		send_str("1");
		set_DDRAM_address(0x42);
		send_str("2");
		set_DDRAM_address(0x44);
		send_str("3");
		set_DDRAM_address(0x46);
		send_str("4");
		set_DDRAM_address(0x48);
		send_str("5");
		set_DDRAM_address(0x4A);
		send_str("6");
		set_DDRAM_address(0x4C);
		send_str("7");
		if (set[1]==1){lvl_menu.addres=0x41;}
		else if (set[1]==2){lvl_menu.addres=0x43;}
		else if (set[1]==3){lvl_menu.addres=0x45;}
		else if (set[1]==4){lvl_menu.addres=0x47;}
		else if (set[1]==5){lvl_menu.addres=0x49;}
		else if (set[1]==6){lvl_menu.addres=0x4B;}
		else if (set[1]==7){lvl_menu.addres=0x4D;}
	}
	else if (lvl_menu.menu_poz==5)
	{
		lvl_menu.addres=0x42;
		set_DDRAM_address(0x00);
		send_str(lvl_menu.intens1);
		char qwe;
		set_DDRAM_address(0x40);
		send_str("  ");
		set_DDRAM_address(0x40);
		if (set[2]>9){qwe=set[2]/10+'0';write_data(qwe);qwe=set[2]-set[2]/10*10+'0';write_data(qwe);}
		else {qwe=set[2]+'0';write_data(qwe);}
		
	}
	else if (lvl_menu.menu_poz==7)
	{
		set_DDRAM_address(0x00);
		send_str(lvl_menu.el1);
		set_DDRAM_address(0x40);
		send_str("OFF");
		set_DDRAM_address(0x45);
		send_str("ON");
		if (set[3]==0){lvl_menu.addres=0x43;}else if (set[3]==1){lvl_menu.addres=0x47;}
	}
}
