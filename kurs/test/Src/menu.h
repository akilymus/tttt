#ifndef MENU_H
#define MENU_H
#define left_butt HAL_GPIO_ReadPin(left_GPIO_Port,left_Pin)
#define right_butt HAL_GPIO_ReadPin(right_GPIO_Port,right_Pin)
#define up_butt HAL_GPIO_ReadPin(up_GPIO_Port,up_Pin)
#define down_butt HAL_GPIO_ReadPin(down_GPIO_Port,down_Pin)
#define enter_butt HAL_GPIO_ReadPin(enter_GPIO_Port,enter_Pin)
void draw_lvl (void);
struct BUTTONS
{
	uint8_t press;
	uint8_t count;
};
struct MENU
{
	uint8_t menu_poz;
	uint8_t addres;
	char *mode;
	char *mode1;
	char *mode2;
	char *grb;
	char *grb1;
	char *intens;
	char *intens1;
	char *el;
	char *el1;
};

#endif
