#include "lcd.h"
#include "stm32f1xx_hal.h"
#include "main.h"

uint8_t delay=0;

void lcd_init (void)
{
	reset_pins();
	HAL_Delay(2);
	function_set(1,1);
	HAL_Delay(2);
	function_set(1,1);
	HAL_Delay(2);
	displayOnOff(1,0,0);
	HAL_Delay(2);
	clear_display();
	HAL_Delay(2);
	reset_pins();
}

void function_set (int Nbit, int Fbit)
{
	HAL_GPIO_WritePin(RS_GPIO_Port,RS_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(RW_GPIO_Port,RW_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB7_GPIO_Port,DB7_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB6_GPIO_Port,DB6_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB5_GPIO_Port,DB5_Pin,GPIO_PIN_SET);
	HAL_GPIO_WritePin(DB4_GPIO_Port,DB4_Pin,GPIO_PIN_SET);
	HAL_GPIO_WritePin(DB1_GPIO_Port,DB1_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB0_GPIO_Port,DB0_Pin,GPIO_PIN_RESET);
	if(Nbit==1){HAL_GPIO_WritePin(DB3_GPIO_Port,DB3_Pin,GPIO_PIN_SET);}
	else {HAL_GPIO_WritePin(DB3_GPIO_Port,DB3_Pin,GPIO_PIN_RESET);}
	if(Fbit==1){HAL_GPIO_WritePin(DB2_GPIO_Port,DB2_Pin,GPIO_PIN_SET);}
	else {HAL_GPIO_WritePin(DB2_GPIO_Port,DB2_Pin,GPIO_PIN_RESET);}
	data_read_write();
}
void write_data(uint8_t data)
{
	HAL_GPIO_WritePin(RS_GPIO_Port,RS_Pin,GPIO_PIN_SET);
	HAL_GPIO_WritePin(RW_GPIO_Port,RW_Pin,GPIO_PIN_RESET);
	if (data & BIT_7_MASK){HAL_GPIO_WritePin(DB7_GPIO_Port,DB7_Pin,GPIO_PIN_SET);}
	else{HAL_GPIO_WritePin(DB7_GPIO_Port,DB7_Pin,GPIO_PIN_RESET);}
	if (data & BIT_6_MASK){HAL_GPIO_WritePin(DB6_GPIO_Port,DB6_Pin,GPIO_PIN_SET);}
	else{HAL_GPIO_WritePin(DB6_GPIO_Port,DB6_Pin,GPIO_PIN_RESET);}
	if (data & BIT_5_MASK){HAL_GPIO_WritePin(DB5_GPIO_Port,DB5_Pin,GPIO_PIN_SET);}
	else{HAL_GPIO_WritePin(DB5_GPIO_Port,DB5_Pin,GPIO_PIN_RESET);}
	if (data & BIT_4_MASK){HAL_GPIO_WritePin(DB4_GPIO_Port,DB4_Pin,GPIO_PIN_SET);}
	else{HAL_GPIO_WritePin(DB4_GPIO_Port,DB4_Pin,GPIO_PIN_RESET);}
	if (data & BIT_3_MASK){HAL_GPIO_WritePin(DB3_GPIO_Port,DB3_Pin,GPIO_PIN_SET);}
	else{HAL_GPIO_WritePin(DB3_GPIO_Port,DB3_Pin,GPIO_PIN_RESET);}
	if (data & BIT_2_MASK){HAL_GPIO_WritePin(DB2_GPIO_Port,DB2_Pin,GPIO_PIN_SET);}
	else{HAL_GPIO_WritePin(DB2_GPIO_Port,DB2_Pin,GPIO_PIN_RESET);}
	if (data & BIT_1_MASK){HAL_GPIO_WritePin(DB1_GPIO_Port,DB1_Pin,GPIO_PIN_SET);}
	else{HAL_GPIO_WritePin(DB1_GPIO_Port,DB1_Pin,GPIO_PIN_RESET);}
	if (data & BIT_0_MASK){HAL_GPIO_WritePin(DB0_GPIO_Port,DB0_Pin,GPIO_PIN_SET);}
	else{HAL_GPIO_WritePin(DB0_GPIO_Port,DB0_Pin,GPIO_PIN_RESET);}
	data_read_write();
}
void send_str(char *str)
{
	uint8_t data;
	while (*str){data=*str++;write_data(data);}
}

void data_read_write(void)
{
	HAL_GPIO_WritePin(E_GPIO_Port,E_Pin,GPIO_PIN_SET);
	while (delay<200){delay++;}
	delay=0;
	HAL_GPIO_WritePin(E_GPIO_Port,E_Pin,GPIO_PIN_RESET);
	reset_pins();
}
void displayOnOff(int Dbit, int Cbit, int Bbit)
{
	HAL_GPIO_WritePin(RS_GPIO_Port,RS_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(RW_GPIO_Port,RW_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB7_GPIO_Port,DB7_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB6_GPIO_Port,DB6_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB5_GPIO_Port,DB5_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB4_GPIO_Port,DB4_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB3_GPIO_Port,DB3_Pin,GPIO_PIN_SET);
	if (Dbit == 1){HAL_GPIO_WritePin(DB2_GPIO_Port,DB2_Pin,GPIO_PIN_SET);}
	else{HAL_GPIO_WritePin(DB2_GPIO_Port,DB2_Pin,GPIO_PIN_RESET);}
	if (Cbit == 1){HAL_GPIO_WritePin(DB1_GPIO_Port,DB1_Pin,GPIO_PIN_SET);}
	else{HAL_GPIO_WritePin(DB1_GPIO_Port,DB1_Pin,GPIO_PIN_RESET);}
	if (Bbit == 1){HAL_GPIO_WritePin(DB0_GPIO_Port,DB0_Pin,GPIO_PIN_SET);}
	else{HAL_GPIO_WritePin(DB0_GPIO_Port,DB0_Pin,GPIO_PIN_RESET);}
	data_read_write();
}
void clear_display(void)
{
	HAL_GPIO_WritePin(RS_GPIO_Port,RS_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(RW_GPIO_Port,RW_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB7_GPIO_Port,DB7_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB6_GPIO_Port,DB6_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB5_GPIO_Port,DB5_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB4_GPIO_Port,DB4_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB3_GPIO_Port,DB3_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB2_GPIO_Port,DB2_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB1_GPIO_Port,DB1_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB0_GPIO_Port,DB0_Pin,GPIO_PIN_SET);
	
	data_read_write();
}
void reset_pins(void)
{
	HAL_GPIO_WritePin(RS_GPIO_Port,RS_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(RW_GPIO_Port,RW_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB7_GPIO_Port,DB7_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB6_GPIO_Port,DB6_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB5_GPIO_Port,DB5_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB4_GPIO_Port,DB4_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB3_GPIO_Port,DB3_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB2_GPIO_Port,DB2_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB1_GPIO_Port,DB1_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB0_GPIO_Port,DB0_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(E_GPIO_Port,E_Pin,GPIO_PIN_RESET);
}
void set_DDRAM_address(uint8_t address)
{
	HAL_GPIO_WritePin(RS_GPIO_Port,RS_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(RW_GPIO_Port,RW_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(DB7_GPIO_Port,DB7_Pin,GPIO_PIN_SET);
	if (address & BIT_6_MASK){HAL_GPIO_WritePin(DB6_GPIO_Port,DB6_Pin,GPIO_PIN_SET);}
	else{HAL_GPIO_WritePin(DB6_GPIO_Port,DB6_Pin,GPIO_PIN_RESET);}
	if (address & BIT_5_MASK){HAL_GPIO_WritePin(DB5_GPIO_Port,DB5_Pin,GPIO_PIN_SET);}
	else{HAL_GPIO_WritePin(DB5_GPIO_Port,DB5_Pin,GPIO_PIN_RESET);}
	if (address & BIT_4_MASK){HAL_GPIO_WritePin(DB4_GPIO_Port,DB4_Pin,GPIO_PIN_SET);}
	else{HAL_GPIO_WritePin(DB4_GPIO_Port,DB4_Pin,GPIO_PIN_RESET);}
	if (address & BIT_3_MASK){HAL_GPIO_WritePin(DB3_GPIO_Port,DB3_Pin,GPIO_PIN_SET);}
	else{HAL_GPIO_WritePin(DB3_GPIO_Port,DB3_Pin,GPIO_PIN_RESET);}
	if (address & BIT_2_MASK){HAL_GPIO_WritePin(DB2_GPIO_Port,DB2_Pin,GPIO_PIN_SET);}
	else{HAL_GPIO_WritePin(DB2_GPIO_Port,DB2_Pin,GPIO_PIN_RESET);}
	if (address & BIT_1_MASK){HAL_GPIO_WritePin(DB1_GPIO_Port,DB1_Pin,GPIO_PIN_SET);}
	else{HAL_GPIO_WritePin(DB1_GPIO_Port,DB1_Pin,GPIO_PIN_RESET);}
	if (address & BIT_0_MASK){HAL_GPIO_WritePin(DB0_GPIO_Port,DB0_Pin,GPIO_PIN_SET);}
	else{HAL_GPIO_WritePin(DB0_GPIO_Port,DB0_Pin,GPIO_PIN_RESET);}
	data_read_write();
}
