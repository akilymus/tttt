#ifndef LCD_H
#define LCD_H
#include "stm32f1xx_hal.h"

void function_set (int Nbit, int Fbit);
void data_read_write(void);
void write_data(uint8_t data);
void displayOnOff(int Dbit, int Cbit, int Bbit);
void clear_display(void);
void reset_pins(void);
void set_DDRAM_address(uint8_t address);
void send_str(char *str);
void lcd_init (void);

#define BIT_7_MASK (0x80)
#define BIT_6_MASK (0x40)
#define BIT_5_MASK (0x20)
#define BIT_4_MASK (0x10)
#define BIT_3_MASK (0x08)
#define BIT_2_MASK (0x04)
#define BIT_1_MASK (0x02)
#define BIT_0_MASK (0x01)
#endif
